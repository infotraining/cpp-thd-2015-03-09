#include <iostream>
#include <fstream>
#include <string>
#include "../../utils.h"

using namespace std;

class Logger
{
    ofstream file_;
    active_object ao; // must be the last attribute

public:
    Logger(const string& log_file_name)
    {
        file_.open(log_file_name);
    }

    void log(const string& msg)
    {
        ao.send([this, msg] ()
        {
             file_ << msg << endl;
             file_ << flush;
        } );
    }
};

void test_logger(Logger& log, int id)
{
    for (int i = 0 ; i < 1000 ; ++i)
    {
        log.log("Message " + to_string(i) + " from " + to_string(id));
    }
}

int main()
{
    Logger log("data.log");
    scoped_thread th1(test_logger, ref(log), 1);
    scoped_thread th2(test_logger, ref(log), 2);
    return 0;
}

