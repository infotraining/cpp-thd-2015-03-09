#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>
#include <algorithm>
#include <queue>

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(chrono::milliseconds(50));
        q.push(i);
        cout << "just produced " << i << endl;
    }
}

void consumer(int id)
{
    for (;;)
    {
        int msg;
        q.pop(msg);
        // bool succes = q.pop_nowait(msg);
        cout << id << " just got " << msg << endl;
    }
}

int main()
{
    scoped_thread prod(producer);
    scoped_thread cons1(consumer, 1);
    scoped_thread cons2(consumer, 2);
    return 0;
}
