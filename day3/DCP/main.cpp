#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>
#include <algorithm>
#include <queue>

using namespace std;

class Service
{
public:
    virtual void run() =0;
    virtual ~Service() {}
};

class RealService : public Service
{
    string url_;
public:
    RealService(const string& url) : url_(url)
    {
        cout << "Creating service...." << endl;
        this_thread::sleep_for(1s);
        cout << "Service ready" << endl;
    }

    void run()
    {
        cout << "RealService::run" << endl;
    }
};

namespace not_working
{

class ProxyService : public Service
{
    RealService* real_service {nullptr};
    string url_;
    mutex mtx;
public:
    ProxyService(const string& url) : url_(url)
    {
    }

    void run()
    {
        if (real_service == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            if (real_service == nullptr)
                real_service = new RealService(url_);
        }
        real_service->run();
    }
};
}

namespace atomic_working
{

class ProxyService : public Service
{
    atomic<RealService*> real_service {nullptr};
    string url_;
    mutex mtx;
public:
    ProxyService(const string& url) : url_(url)
    {
    }

    void run()
    {
        if (real_service.load() == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            if (real_service.load() == nullptr)
                real_service.store(new RealService(url_));
        }
        (*real_service).run();
    }
};

}

namespace call_once_working
{

class ProxyService : public Service
{
    RealService* real_service {nullptr};
    string url_;
    once_flag flag;
public:
    ProxyService(const string& url) : url_(url)
    {
    }

    void run()
    {
        call_once(flag, [this] () { real_service = new RealService(url_);});
        real_service->run();
    }
};
}

namespace static_working
{

class ProxyService : public Service
{
    static RealService real_service;
    string url_;

public:
    ProxyService(const string& url) : url_(url)
    {
    }

    RealService get_instance()
    {
        static RealService real_service(url_);
        return real_service;
    }

    void run()
    {

        get_instance().run();
    }
};
}

using namespace static_working;

int main()
{
    cout << "Hello World!" << endl;
    ProxyService service("www.wp.pl");
    vector<scoped_thread> threads;
    for(int i = 0 ; i < 10 ; ++i)
        threads.emplace_back([&service] {service.run();});
    return 0;
}

