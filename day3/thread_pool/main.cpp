#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>
#include <algorithm>
#include <queue>

using namespace std;

void hello()
{
    cout << "Hello" << endl;
}

struct test_functor
{
    void operator()()
    {
        cout << "From functor" << endl;
    }
};

int question()
{
    this_thread::sleep_for(chrono::milliseconds(500));
    return 42;
}


int main()
{
    thread_pool tp(4);
    tp.submit(hello);
    for (int i = 1 ; i < 20 ;++i)
    {
        tp.submit([i] () { this_thread::sleep_for(chrono::milliseconds(500)); cout << "Lambda id = " << i << endl;});
    }
    tp.submit(test_functor());

    future<int> res = tp.async(question);

    cout << "Answer is " << res.get() << endl;
    return 0;
}

