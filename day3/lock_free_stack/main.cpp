#include <iostream>
#include "../../utils.h"
#include <mutex>
#include <atomic>

using namespace std;

struct Node
{
    Node* prev;
    int data;
};

class Stack
{
    atomic<Node*> head{nullptr};
    mutex mtx;
public:
    void push(int item)
    {
        Node* new_node = new Node;
        new_node->data = item;
        new_node->prev = head.load();
        while(!head.compare_exchange_weak(new_node->prev, new_node));
    }

    int pop()
    {
        Node* old_node = head.load();
        while(!head.compare_exchange_weak(old_node, old_node->prev));
        int res = old_node->data;
        delete old_node;
        return res;
    }
};

void test_stack(Stack& s)
{
    for (int i = 0 ; i < 10000 ; ++i)
    {
        s.push(i);
    }
    for (int i = 0 ; i < 10000 ; ++i)
    {
        cout << s.pop() << ", ";
    }
    cout << endl;
}

int main()
{
    cout << "Stack" << endl;
    Stack s;

    scoped_thread th1(test_stack, ref(s));
    scoped_thread th2(test_stack, ref(s));
    scoped_thread th3(test_stack, ref(s));
    scoped_thread th4(test_stack, ref(s));

    return 0;
}

