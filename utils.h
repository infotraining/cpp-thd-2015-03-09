#ifndef UTILS_H
#define UTILS_H
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <future>

class scoped_thread
{
    std::thread th_;
public:
    scoped_thread(std::thread th) : th_(move(th)) {}
    // not copyable
    scoped_thread(const scoped_thread&) = delete;
    scoped_thread& operator=(const scoped_thread&) = delete;
    // default move semantic
    scoped_thread(scoped_thread&& old) : th_(move(old.th_))
    {
    }

    template<typename... T>
    scoped_thread(T&&... args) : th_(std::forward<T>(args)...)
    {
    }

    scoped_thread& operator=(scoped_thread&&) = default;
    ~scoped_thread()
    {
        if (th_.joinable()) th_.join();
    }
};

template<typename T>
class thread_safe_queue
{
    std::queue<T> q;
    std::mutex mtx;
    std::condition_variable cond;
public:
    void push(T item)
    {
        std::lock_guard<std::mutex> lg(mtx);
        q.push(item);
        cond.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> ul(mtx);
        cond.wait(ul, [this] () {return !q.empty();});
        item = q.front();
        q.pop();
    }

    bool pop_nowait(T& item)
    {
        std::unique_lock<std::mutex> ul(mtx);
        if (q.empty()) return false;
        item = q.front();
        q.pop();
        return true;
    }
};

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q;
    std::vector<scoped_thread> workers;

public:
    thread_pool(int n_of_workers)
    {
        for( int i = 0 ; i < n_of_workers ; ++i)
        {
            workers.emplace_back([this] ()
            {
               for(;;)
               {
                   task_t task;
                   q.pop(task);
                   if(!task) return;
                   task();
               }
            });
        }
    }

    void submit(task_t task)
    {
        if(task)
            q.push(task);
    }

    ~thread_pool()
    {
        for (auto i = 0u ; i < workers.size() ; ++i)
            q.push(task_t());
    }

    template<typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        using res_t = decltype(f());
        auto task = std::make_shared<std::packaged_task<res_t()>>(f);
        std::future<res_t> res = task->get_future();
        q.push([task] () { (*task)(); });
        return res;
    }
};

class active_object
{
    thread_safe_queue<task_t> q;
    scoped_thread servant;

public:
    active_object()
    {
        servant = std::move(scoped_thread([this] ()
            {
               for(;;)
               {
                   task_t task;
                   q.pop(task);
                   if(!task) return;
                   task();
               }
            })
        );
    }

    void send(task_t task)
    {
        if(task)
            q.push(task);
    }

    ~active_object()
    {
        q.push(task_t());
    }
};


#endif // UTILS_H

