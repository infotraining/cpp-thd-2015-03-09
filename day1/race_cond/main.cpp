#include <iostream>
#include <thread>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

class spinlock
{
    std::atomic_flag flag;
public:
    spinlock() : flag(ATOMIC_FLAG_INIT) {}
    bool try_lock()
    {
        return !flag.test_and_set();
    }

    void lock()
    {
        while(!flag.test_and_set());
    }

    void unlock()
    {
        flag.clear();
    }
};

class mtx_guard
{
    mutex& mtx;
public:
    mtx_guard(const mtx_guard&) = delete;
    mtx_guard& operator=(const mtx_guard&) = delete;
    mtx_guard(mutex& m) : mtx(m)
    {
        mtx.lock();
    }
    ~mtx_guard()
    {
        mtx.unlock();
    }
};

atomic<long> counter_atomic{};
long counter{};
long counter_mutex{};
mutex mtx;
spinlock sl;

void increase()
{
    for (int i = 0 ; i < 10000 ; ++i)
        ++counter;
}

void increase_atomic()
{
    for (int i = 0 ; i < 10000 ; ++i)
        counter_atomic.fetch_add(1);
}

void increase_mtx()
{
    for (int i = 0 ; i < 10000 ; ++i)
    {
        lock_guard<mutex> lg(mtx);
        ++counter;
        if (counter == 1000)
        {
            return;
        }
    }
}

int main()
{   
    auto start  = chrono::high_resolution_clock::now();
    {
        vector<scoped_thread> thds;
        for (int i = 0 ; i < 4 ; ++i)
            thds.emplace_back(increase);
    }
    auto end  = chrono::high_resolution_clock::now();
    cout << "elapsed for counter ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;
    cout << "Counter value = " << counter << endl;

    start  = chrono::high_resolution_clock::now();
    {
        vector<scoped_thread> thds;
        for (int i = 0 ; i < 4 ; ++i)
            thds.emplace_back(increase_atomic);
    }

    end  = chrono::high_resolution_clock::now();
    cout << "elapsed for atomic counter ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;
    cout << "Atomic Counter value = " << counter_atomic.load() << endl;

    counter = 0;
    start  = chrono::high_resolution_clock::now();
    {
        vector<scoped_thread> thds;
        for (int i = 0 ; i < 4 ; ++i)
            thds.emplace_back(increase_mtx);
    }

    end  = chrono::high_resolution_clock::now();
    cout << "elapsed for mtx counter ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;
    cout << "Mtx Counter value = " << counter << endl;
    return 0;
}

