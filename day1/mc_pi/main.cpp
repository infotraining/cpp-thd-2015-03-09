#include <random>
#include <iostream>
#include <thread>
#include <chrono>
#include "../../utils.h"

using namespace std;

void calc_pi(long N, long& counter)
{
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_real_distribution<long double> dis(0, 1);
    long double x, y;
    for (int n = 0; n < N; ++n) {
        x = dis(gen);
        y = dis(gen);
        if (x*x + y*y < 1)
            ++counter;
    }
}

int main()
{
    long N = 100000000;
    //int n_of_threads = thread::hardware_concurrency();
    int n_of_threads = 16;
    cout << "Threads = " << n_of_threads << endl;
    vector<long> counters(n_of_threads);

    auto start  = chrono::high_resolution_clock::now();
    {
        vector<scoped_thread> threads;
        for (int i = 0 ; i < n_of_threads ; ++i)
            threads.emplace_back( calc_pi, N/n_of_threads, ref(counters[i]));
    }
    long counter = accumulate(counters.begin(), counters.end(), 0L);
    auto end  = chrono::high_resolution_clock::now();

    cout << "elapsed for " << n_of_threads << " threads: ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    std::cout << "Pi = " << (double)counter/(double)N * 4 << endl;

}
