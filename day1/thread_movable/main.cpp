#include <iostream>
#include <thread>
#include "../../utils.h"
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
}

void called(int id)
{
    cout << "Called " << id << endl;
}

struct Task
{
    void operator()(int id)
    {
        cout << "Hello from task " << id << endl;
    }
};

class Buff
{
    vector<int> buff_;
public:
    Buff() {}
    void assign(const vector<int>& src)
    {
        buff_.assign(src.begin(), src.end());
    }

    vector<int> data() const
    {
        return buff_;
    }
};

int main()
{
    cout << "Hello World!" << endl;
//    thread th1(hello);
//    th1.join();
    scoped_thread th1{thread(hello)};
    scoped_thread th2{called, 10};
    Task t;
    scoped_thread th3(t, 13);

    vector<int> src {1,2,3,4,5};
    Buff b;
    //b.assign(src); too long
    {
        scoped_thread th4(&Buff::assign, &b, src);
    }
    {
        scoped_thread th4([&] () { b.assign(src);});
    }
    for (auto& i : b.data())
    {
        cout << i << ", ";
    }
    cout << endl;

    vector<scoped_thread> threads;
    for(int i = 0 ; i < 10 ;++i )
        threads.emplace_back(called, i);

    //for(auto& th : threads) th.join();


    return 0;
}

