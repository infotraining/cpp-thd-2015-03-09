#include <iostream>
#include <thread>

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
}

void called(int id)
{
    cout << "Called " << id << endl;
}

void question(int& answer)
{
    answer = 42;
}

void test_reference(const int& id)
{
    this_thread::sleep_for(chrono::milliseconds(200));
    cout << "my id is " << id << endl;
}

thread generate_thread()
{
    int id = 123;
    return thread(&test_reference, cref(id));
}

int main()
{
    cout << "Hello World!" << endl;
    thread th1(&hello);
    thread th4(move(th1));
    cout << "is th1 joinable " << th1.joinable()  << endl;
    cout << "is th4 joinable " << th4.joinable()  << endl;
    thread th2(&called, 10);
    int result{};
    thread th3(&question, ref(result));
    cout << "after launching a thread" << endl;
    th4.join();
    th2.join();
    th3.join();

    thread th6( [result] () { cout << "From lambda " << result << endl;});
    th6.join();
    //returning from function
    thread th5(generate_thread());
    th5.join();
    cout << "result = " << result << endl;
}

