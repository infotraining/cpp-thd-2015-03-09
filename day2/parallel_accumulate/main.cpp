#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>
#include <algorithm>
#include <deque>
#include <list>

using namespace std;

template<typename It, typename T>
future<T> parallel_accumulate_cpp14(It first, It last, T init)
{
    int N = max(thread::hardware_concurrency(), 1u);
    vector<future<T>> results;

    size_t block_size = distance(first, last)/N;
    It block_start = first;

    for(int i = 0 ; i < N ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if (i == N-1) block_end = last;
        results.push_back(async(launch::async,
                                accumulate<It, T>, block_start, block_end, T()));
        block_start = block_end;
    }

    return async(launch::deferred,
                 [results=move(results), init] () mutable { return accumulate(results.begin(), results.end(), init,
                      [] (T a, future<T>& b) { return a + b.get();});
                                         }
                );

}

template<typename It, typename T>
future<T> parallel_accumulate(It first, It last, T init)
{
    int N = max(thread::hardware_concurrency(), 1u);
    auto results = make_shared<vector<future<T>>>();

    size_t block_size = distance(first, last)/N;
    It block_start = first;

    for(int i = 0 ; i < N ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if (i == N-1) block_end = last;
        results->push_back(async(launch::async,
                                accumulate<It, T>, block_start, block_end, T()));
        block_start = block_end;
    }

    return async(launch::deferred,
                 [results, init] () mutable { return accumulate(results->begin(), results->end(), init,
                      [] (T a, future<T>& b) { return a + b.get();});
                                         }
                );

}


int main()
{
    vector<long> v;
    for (long i = 0 ; i < 100000000 ; ++i)
        v.push_back(i);

    auto start = chrono::high_resolution_clock::now();
    cout << "Sum = " << std::accumulate(v.begin(), v.end(), 0L) << endl;
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    start = chrono::high_resolution_clock::now();
    cout << "Sum by parallel accumulate = ";
    cout << parallel_accumulate(v.begin(), v.end(), 0L).get() << endl;
    end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;
    return 0;
}

