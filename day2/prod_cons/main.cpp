#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>
#include <algorithm>
#include <queue>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(chrono::milliseconds(10));
        lock_guard<mutex> lg(mtx);
        q.push(i);
        cout << "just produced " << i << endl;
        cond.notify_one();
    }
    q.push(-1);
    cond.notify_all();
}

void consumer(int id)
{
    for (;;)
    {
        unique_lock<mutex> lg(mtx);
//        while(q.empty())
//        {
//            cond.wait(lg);
//        }
        cond.wait(lg, [] { return !q.empty();});
        int msg = q.front();
        if (msg == -1) return;
        q.pop();
        cout << id << " just got " << msg << endl;

    }
}

int main()
{
    scoped_thread prod(producer);
    scoped_thread cons1(consumer, 1);
    scoped_thread cons2(consumer, 2);
    return 0;
}

