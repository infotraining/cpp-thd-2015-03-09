#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"

using namespace std;

void can_throw(int id)
{

    cout << "worker " << id << " working" << endl;
    this_thread::sleep_for(chrono::seconds(1));
    if(id == 13) throw std::runtime_error("bad luck");

}

void worker(int id, exception_ptr& eptr)
{
    try{
    for(int i = 0 ; i < 5 ; ++i)
        can_throw(id);
    }
    catch(...)
    {
        cerr << "thread has got an exception" << endl;
        eptr = current_exception();
    }
}

int main()
{
    exception_ptr th1_ptr;
    exception_ptr th2_ptr;
    {
        scoped_thread th1(worker, 13, ref(th1_ptr));
        scoped_thread th2(worker, 2, ref(th2_ptr));
        //worker(13);
    }
    try
    {
        if(th1_ptr)
        {
            rethrow_exception(th1_ptr);
        }
    }

    catch(std::runtime_error& err)
    {
        cerr << "exception catched - " << err.what() << endl;
    }

    return 0;
}

