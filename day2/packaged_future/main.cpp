#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>
#include <functional>

using namespace std;

auto question() -> int
{
    this_thread::sleep_for(chrono::seconds(1));
    return 42;
}

int can_throw(int id)
{
    cout << "worker " << id << " working" << endl;
    this_thread::sleep_for(chrono::seconds(1));
    if(id == 13) throw std::runtime_error("bad luck");
    return id;
}

template<typename F>
class my_packaged_task
{
    std::function<F> f_;
    std::promise<decltype(f_())> p_;
public:
    my_packaged_task(std::function<F> f) : f_(f)
    {
    }

    void operator ()()
    {
        try{
            p_.set_value(f_());
        }
        catch(...)
        {
            p_.set_exception( current_exception() );
        }
    }

    auto get_future() -> future<decltype(f_())>
    {
        return p_.get_future();
    }
};

int main()
{
    future<int> result;

//    std::function<int()> fun = question;
//    fun = [] () { return 77; };
//    cout << "result = " << fun() << endl;

    //result = async(launch::async, question);

    //packaged_task<int()> pt(question);
    my_packaged_task<int()> pt(question);
    result = pt.get_future();
    scoped_thread st(move(pt));

    my_packaged_task<int()> pt2( [] () { return can_throw(13);} );
    future<int> res2 = pt2.get_future();
    scoped_thread st2(move(pt2));
    try{
    res2.get();
    }
    catch(...)
    {
        cerr << "error" << endl;
    }

    cout << "Got : " << result.get() << endl;
}

