#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include "../../utils.h"

using namespace std;
timed_mutex mtx;

void worker_obsolete()
{
    cout << "Worker started " << this_thread::get_id() << endl;
    for(;;)
    {
        if (mtx.try_lock())
        {
            cout << "I'v got mutex " << this_thread::get_id() << endl;
            this_thread::sleep_for(2s);
            mtx.unlock();
            return;
        }
        else{
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
            this_thread::sleep_for(500ms);
        }
    }
}

void worker()
{
    cout << "Worker started " << this_thread::get_id() << endl;
    unique_lock<timed_mutex> ul(mtx, try_to_lock);
    if( !ul.owns_lock() )
    {
        do {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
        }
        while(!ul.try_lock_for(chrono::milliseconds(500)));
    }
    cout << "I have lock " << this_thread::get_id() << endl;
    this_thread::sleep_for(2s);
}

int main()
{
    scoped_thread th1(worker);
    scoped_thread th2(worker);
    return 0;
}

