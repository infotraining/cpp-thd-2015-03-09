#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>
#include <algorithm>

using namespace std;

class Data_bool
{
    vector<int> data_;
    atomic<bool> is_ready{false};
public:
    void read()
    {
        cout << "Reading data" << endl;
        cout << "using lock-free bool? " << is_ready.is_lock_free() << endl;
        this_thread::sleep_for(chrono::seconds(10));
        data_.resize(20);
        generate(data_.begin(), data_.end(), [] {return rand() % 100;});
        is_ready = true;

    }

    void process()
    {
        for(;;)
        {
            if(is_ready)
            {
                cout << "processing";
                cout << ", sum = " << accumulate(data_.begin(), data_.end(), 0);
                cout << endl;
                return;
            }
            //this_thread::sleep_for(chrono::microseconds(100));
            this_thread::yield();
        }
    }
};

class Data
{
    vector<int> data_;
    bool is_ready{false};
    condition_variable cond;
    mutex mtx;
public:
    void read()
    {
        cout << "Reading data" << endl;
        this_thread::sleep_for(chrono::seconds(1));
        data_.resize(20);
        generate(data_.begin(), data_.end(), [] {return rand() % 100;});
        {
            lock_guard<mutex> lg(mtx);
            is_ready = true;
        }
        cond.notify_all();
    }

    void process()
    {
        for(;;)
        {
            unique_lock<mutex> lk(mtx);
            while(!is_ready)
            {
                cond.wait(lk);
            }
            cout << "processing";
            cout << ", sum = " << accumulate(data_.begin(), data_.end(), 0);
            cout << endl;
            return;
        }
    }
};


int main()
{
    cout << "starting" << endl;
    Data data;
    scoped_thread th_read(&Data::read, &data);
    scoped_thread th_process1(&Data::process, &data);
    scoped_thread th_process2(&Data::process, &data);
    return 0;
}

