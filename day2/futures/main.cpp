#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <exception>
#include "../../utils.h"
#include <future>

using namespace std;

void can_throw(int id)
{
    cout << "worker " << id << " working" << endl;
    this_thread::sleep_for(chrono::seconds(1));
    if(id == 13) throw std::runtime_error("bad luck");
}

auto question() -> int
{
    this_thread::sleep_for(chrono::seconds(1));
    return 42;
}

auto main() -> int
{
    cout << "starting calculations" << endl;
    int result{};
    {
        scoped_thread th1([&result] () -> void { result = question();});
        //cout << "The result = " << result << endl; <- too early...
    }
    cout << "The result = " << result << endl;

    vector<future<int>> results;
    for ( int i = 0 ; i < 20 ; ++i)
    {
        results.push_back(async(launch::async, question));
    }
    for (auto& res : results)
    {
        res.wait();
        cout << "The result = " << res.get() << endl;
    }

    // exceptions in futures
    future<void> cres = async(launch::async, can_throw, 13);
    cres.wait();
    try
    {
        cres.get();
    }
    catch(std::runtime_error& err)
    {
        cerr << "got error: " << err.what() << endl;
    }

    cout << "Finished " << endl;

    return 0;
}

